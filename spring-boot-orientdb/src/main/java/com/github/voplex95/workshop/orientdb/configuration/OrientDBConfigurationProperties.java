package com.github.voplex95.workshop.orientdb.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
@Configuration
@ConfigurationProperties(prefix = "orientdb")
public class OrientDBConfigurationProperties {

    @NotBlank
    private String url;

    @NotBlank
    private String database;

    @NotBlank
    private String user;

    @NotBlank
    private String password;

}
