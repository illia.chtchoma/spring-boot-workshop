package com.github.voplex95.workshop.orientdb.web.controller;

import com.github.voplex95.workshop.orientdb.model.PersonImpl;
import com.github.voplex95.workshop.orientdb.service.PersonService;
import com.github.voplex95.workshop.orientdb.web.dto.PersonDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/people")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;
    private final ModelMapper modelMapper;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PersonDto> findAll() {
        return personService.findAll()
                .stream()
                .map(p -> modelMapper.map(p, PersonDto.class))
                .collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void add(@Valid @RequestBody PersonDto personDto) {
        personService.add(modelMapper.map(personDto, PersonImpl.class));
    }

}
