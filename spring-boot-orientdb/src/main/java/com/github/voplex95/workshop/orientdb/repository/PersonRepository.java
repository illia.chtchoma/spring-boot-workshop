package com.github.voplex95.workshop.orientdb.repository;

import com.github.voplex95.workshop.orientdb.model.Person;

import java.util.List;

public interface PersonRepository {

    List<Person> findAll();
    Person save(Person person);

}
