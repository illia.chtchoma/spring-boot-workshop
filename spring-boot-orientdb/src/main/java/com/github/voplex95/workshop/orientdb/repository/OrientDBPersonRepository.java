package com.github.voplex95.workshop.orientdb.repository;

import com.github.voplex95.workshop.orientdb.model.Person;
import com.google.common.collect.Lists;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.frames.FramedTransactionalGraph;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class OrientDBPersonRepository implements PersonRepository {

    private final FramedTransactionalGraph<OrientGraph> graph;

    @Override
    public List<Person> findAll() {
        return Lists.newArrayList(graph.query().vertices(Person.class));
    }

    @Override
    public Person save(Person person) {
        var vertex = graph.addVertex(UUID.randomUUID(), Person.class);
        vertex.setName(person.getName());
        vertex.setSurname(person.getSurname());
        graph.commit();
        return vertex;
    }

}
