package com.github.voplex95.workshop.orientdb.model;

import lombok.Data;

@Data
public class PersonImpl implements Person {

    private String name;
    private String surname;

}
