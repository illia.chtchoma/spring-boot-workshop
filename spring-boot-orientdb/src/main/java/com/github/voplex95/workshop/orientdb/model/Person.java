package com.github.voplex95.workshop.orientdb.model;

import com.tinkerpop.frames.Property;

public interface Person {

    @Property("name")
    String getName();

    @Property("name")
    void setName(String name);

    @Property("surname")
    String getSurname();

    @Property("surname")
    void setSurname(String surname);

}
