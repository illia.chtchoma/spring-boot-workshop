package com.github.voplex95.workshop.orientdb.configuration;

import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.frames.FramedGraphFactory;
import com.tinkerpop.frames.FramedTransactionalGraph;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class OrientDBConfiguration {

    private final OrientDBConfigurationProperties properties;

    @Bean(destroyMethod = "shutdown")
    public OrientGraph orientGraph() {
        return new OrientGraph(
                String.join("/", properties.getUrl(), properties.getDatabase()),
                properties.getUser(),
                properties.getPassword());
    }

    @Bean(destroyMethod = "shutdown")
    public FramedTransactionalGraph<OrientGraph> framedGraph(OrientGraph orientGraph) {
        return new FramedGraphFactory().create(orientGraph);
    }

}
