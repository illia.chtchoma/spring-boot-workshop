package com.github.voplex95.workshop.orientdb;

import com.github.voplex95.workshop.orientdb.model.Network;
import com.github.voplex95.workshop.orientdb.model.Person;
import com.github.voplex95.workshop.orientdb.model.Relation;
import com.github.voplex95.workshop.orientdb.repository.NetworkRepository;
import com.github.voplex95.workshop.orientdb.repository.PersonRepository;
import com.microsoft.spring.data.gremlin.query.GremlinTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class TestData {

    private final NetworkRepository networkRepository;
    private final PersonRepository personRepository;
    private final GremlinTemplate gremlinTemplate;

    @PostConstruct
    public void insertTestData() {
        this.networkRepository.deleteAll();

        var firstPerson = new Person();
        firstPerson.setName("Tony");
        firstPerson.setSurname("Stark");

        personRepository.save(firstPerson);

        /*var secondPerson = new Person();
        secondPerson.setName("Steve");
        secondPerson.setSurname("Rogers");

        var thirdPerson = new Person();
        thirdPerson.setName("Peter");
        thirdPerson.setSurname("Parker");

        var fourthPerson = new Person();
        fourthPerson.setName("Black");
        fourthPerson.setSurname("Widow");

        var relation = new Relation();
        relation.setName("Friendship");
        relation.setPersonFrom(firstPerson);
        relation.setPersonTo(secondPerson);

        var network = new Network();
        network.getEdges().add(relation);
        network.getVertexes().add(firstPerson);
        network.getVertexes().add(secondPerson);
        network.getVertexes().add(thirdPerson);
        network.getVertexes().add(fourthPerson);

        this.networkRepository.save(network);*/

    }

}
