package com.github.voplex95.workshop.orientdb.repository;

import com.github.voplex95.workshop.orientdb.model.Person;
import com.microsoft.spring.data.gremlin.repository.GremlinRepository;

public interface PersonRepository extends GremlinRepository<Person, String> {

}
