package com.github.voplex95.workshop.orientdb.repository;

import com.github.voplex95.workshop.orientdb.model.Network;
import com.microsoft.spring.data.gremlin.repository.GremlinRepository;

public interface NetworkRepository extends GremlinRepository<Network, String> {


}
