package com.github.voplex95.workshop.orientdb.service;

import com.github.voplex95.workshop.orientdb.model.Person;

import java.util.List;

public interface PersonService {

    List<Person> findAll();
    Person add(Person person);

}
