package com.github.voplex95.workshop.orientdb.repository;

import com.microsoft.spring.data.gremlin.repository.GremlinRepository;

import javax.management.relation.Relation;

public interface RelationRepository extends GremlinRepository<Relation, String> {

}
