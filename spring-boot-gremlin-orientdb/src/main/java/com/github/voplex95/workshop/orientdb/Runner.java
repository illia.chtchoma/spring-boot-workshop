package com.github.voplex95.workshop.orientdb;

import com.microsoft.spring.data.gremlin.repository.config.EnableGremlinRepositories;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableGremlinRepositories
public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(Runner.class, args);
    }

}
