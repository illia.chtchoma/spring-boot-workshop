package com.github.voplex95.workshop.orientdb.model;

import com.microsoft.spring.data.gremlin.annotation.GeneratedValue;
import com.microsoft.spring.data.gremlin.annotation.Vertex;
import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
@Vertex
public class Person {

    @Id
    @GeneratedValue
    private String id;
    private String name;
    private String surname;

}
