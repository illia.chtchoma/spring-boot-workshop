package com.github.voplex95.workshop.orientdb.service.impl;

import com.github.voplex95.workshop.orientdb.model.Person;
import com.github.voplex95.workshop.orientdb.repository.PersonRepository;
import com.github.voplex95.workshop.orientdb.service.PersonService;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Override
    public List<Person> findAll() {
        return Lists.newArrayList(personRepository.findAll());
    }

    @Override
    public Person add(Person person) {
        return personRepository.save(person);
    }

}
