-- Tables compatible with Axon Framework 4.1

CREATE TABLE `AssociationValueEntry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `associationKey` varchar(255) NOT NULL,
  `associationValue` varchar(255) DEFAULT NULL,
  `sagaId` varchar(255) NOT NULL,
  `sagaType` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX2uqqpmht3w2i368ld2ham2out` (`sagaType`,`associationKey`,`associationValue`),
  KEY `IDXpo4uvnt1l3922m6y62fk73p3f` (`sagaId`,`sagaType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `DomainEventEntry` (
  `globalIndex` bigint(20) NOT NULL AUTO_INCREMENT,
  `eventIdentifier` varchar(255) NOT NULL,
  `metaData` longblob,
  `payload` longblob NOT NULL,
  `payloadRevision` varchar(255) DEFAULT NULL,
  `payloadType` varchar(255) NOT NULL,
  `timeStamp` varchar(255) NOT NULL,
  `aggregateIdentifier` varchar(255) NOT NULL,
  `sequenceNumber` bigint(20) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`globalIndex`),
  UNIQUE KEY `UKdg43ia27ypo1jovw2x64vbwv8` (`aggregateIdentifier`,`sequenceNumber`),
  UNIQUE KEY `UK_k5lt6d2792amnloo7q91njp0v` (`eventIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `SagaEntry` (
  `sagaId` varchar(255) NOT NULL,
  `revision` varchar(255) DEFAULT NULL,
  `sagaType` varchar(255) DEFAULT NULL,
  `serializedSaga` longblob,
  PRIMARY KEY (`sagaId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `SnapshotEventEntry` (
  `aggregateIdentifier` varchar(255) NOT NULL,
  `sequenceNumber` bigint(20) NOT NULL,
  `type` varchar(255) NOT NULL,
  `eventIdentifier` varchar(255) NOT NULL,
  `metaData` longblob,
  `payload` longblob NOT NULL,
  `payloadRevision` varchar(255) DEFAULT NULL,
  `payloadType` varchar(255) NOT NULL,
  `timeStamp` varchar(255) NOT NULL,
  PRIMARY KEY (`aggregateIdentifier`,`sequenceNumber`,`type`),
  UNIQUE KEY `UK_sg7xx45yh4ajlsjd8t0uygnjn` (`eventIdentifier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;