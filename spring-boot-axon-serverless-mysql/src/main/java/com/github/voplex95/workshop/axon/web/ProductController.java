package com.github.voplex95.workshop.axon.web;

import com.github.voplex95.workshop.axon.command.CreateProductCommand;
import com.github.voplex95.workshop.axon.web.dto.CreateProductRequest;
import lombok.RequiredArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/products")
@RequiredArgsConstructor
public class ProductController {

    private final CommandGateway commandGateway;

    @PostMapping
    public void create(@Valid @RequestBody CreateProductRequest request) {
        commandGateway.send(new CreateProductCommand(request.getName()));
    }

}
