package com.github.voplex95.workshop.axon.event;

import lombok.Data;

@Data
public class ProductCreatedEvent {

    private final String productId;

}
