package com.github.voplex95.workshop.axon.configuration;

import org.axonframework.eventsourcing.eventstore.EventStorageEngine;
import org.axonframework.eventsourcing.eventstore.jdbc.JdbcEventStorageEngine;
import org.axonframework.eventsourcing.eventstore.jpa.JpaEventStorageEngine;
import org.axonframework.spring.jdbc.SpringDataSourceConnectionProvider;
import org.axonframework.spring.messaging.unitofwork.SpringTransactionManager;
import org.axonframework.springboot.util.jpa.ContainerManagedEntityManagerProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class AxonConfiguration {

    @Bean
    public EventStorageEngine eventStorageEngine(DataSource dataSource, PlatformTransactionManager transactionManager) {
        return jdbcEventStorageEngine(dataSource, transactionManager);
    }

    /**
     * Event storage engine based on plain JDBC (requires 'spring-boot-starter-jdbc' module)
     * @param dataSource the data source.
     * @param transactionManager the transaction manager.
     * @return JDBC-based event storage engine.
     */
    private EventStorageEngine jdbcEventStorageEngine(DataSource dataSource,
                                                      PlatformTransactionManager transactionManager) {
        return JdbcEventStorageEngine.builder()
                .connectionProvider(new SpringDataSourceConnectionProvider(dataSource))
                .transactionManager(new SpringTransactionManager(transactionManager))
                .build();
    }

    /**
     * Event storage engine that utilizes Hibernate (requires 'spring-boot-starter-data-jpa' module)
     * @param dataSource the data source.
     * @param transactionManager the transaction manager.
     * @return JPA-based event storage engine.
     */
    private EventStorageEngine jpaEventStorageEngine(DataSource dataSource,
                                                     PlatformTransactionManager transactionManager) throws SQLException {
        return JpaEventStorageEngine.builder()
                .dataSource(dataSource)
                .entityManagerProvider(new ContainerManagedEntityManagerProvider())
                .transactionManager(new SpringTransactionManager(transactionManager))
                .build();
    }

}
