package com.github.voplex95.workshop.axon.aggregate;

import com.github.voplex95.workshop.axon.command.CreateProductCommand;
import com.github.voplex95.workshop.axon.event.ProductCreatedEvent;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Data
@Slf4j
@Aggregate
public class Product {

    @AggregateIdentifier
    private String id;
    private String name;

    @CommandHandler
    public Product(CreateProductCommand command) {
        this.id = command.getProductId();
        this.name = command.getName();
        AggregateLifecycle.apply(new ProductCreatedEvent(this.id));
    }

    @EventSourcingHandler
    public void on(ProductCreatedEvent event) {
        log.info("Product with ID {} successfully created", event.getProductId());
    }

}
