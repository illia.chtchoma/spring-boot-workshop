package com.github.voplex95.workshop.axon.web.dto;

import lombok.Data;

@Data
public class ProductDto {

    private String id;
    private String name;
    private Boolean available;

}
