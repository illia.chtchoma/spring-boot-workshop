package com.github.voplex95.workshop.axon.web.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CreateProductRequest {

    @NotBlank
    private String name;

}
