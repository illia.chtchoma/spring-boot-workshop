package com.github.voplex95.workshop.axon.command;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.util.UUID;

@Data
public class CreateProductCommand {

    @TargetAggregateIdentifier
    private final String productId = UUID.randomUUID().toString();
    private final String name;

}
